# -*- coding: UTF-8 -*-
# @Time : 2022/4/29 01:16 
# @Author : HongBo Liu
import rabbitmq
import json
"""
rabbitmq 使用topic模式，不能更改
"""

host = '0.0.0.0'
port = 5672
username = ''
password = ''
exchange = ''
routing_key = ''

# 连接rabbitmq
rq = rabbitmq.connect(host, port, username, password)
# 创建生产者
producer = rq.create_producer(exchange, routing_key)


msg = {"data": {"a": "1", "b": "2"}}
msg2 = {"data": {"c": "3", "d": "4"}}

msg_list = [json.dumps(msg), json.dumps(msg2)]


# 发送数据
producer.send(msg_list)
