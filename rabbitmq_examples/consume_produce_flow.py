# -*- coding: UTF-8 -*-
# @Time : 2022/5/20 下午6:21 
# @Author : 刘洪波

import rabbitmq
import json

"""
流服务，用于组成服务流
1）当服务正常运行无报错时将结果发送至下一个服务的exchange 与 routing_key，
2）当服务运行异常有报错时将结果发送至收集error的exchange 与 routing_key。
"""

host = ''
port = 5672
username = ''
password = ''

connect = rabbitmq.connect(host, port, username, password)

service = connect.consume_produce_flow(consumer_exchange='test.rq.ex', consumer_routing_key='test_001',
                                       producer_next_exchange='test.rq.ex', producer_next_routing_key='test_002',
                                       producer_error_exchange='test.rq.ex', producer_error_routing_key='test_result',
                                       durable=True)


def task(msg):
    try:
        print("接收 {} 成功:".format(msg))
        msg = json.loads(msg)
        return [json.dumps({'r1': msg.get('data') + 1})], True
    except Exception as e:
        return [json.dumps({'error': str(e)})], False


service.run(task=task)
