# -*- coding: UTF-8 -*-
# @Time : 2022/5/10 下午3:52 
# @Author : 刘洪波
import json
import rabbitmq
import time
"""
rabbitmq 默认使用topic模式，不能更改
该示例是 rabbitmq 服务端，先消费在生产
"""

host = '0.0.0.0'
port = 5672
username = ''
password = ''


consumer_exchange = ''
consumer_routing_key = ''
producer_exchange = consumer_exchange
producer_routing_key = ''

connect = rabbitmq.connect(host, port, username, password)

# 使用服务端, 推荐使用随机模式
# 随机模式：生产时使用random_exchange, random_routing_key
service = connect.consume_produce(consumer_exchange, consumer_routing_key, durable=True)

# 生产时 使用固定exchange, routing_key
# service = connect.consume_produce(consumer_exchange, consumer_routing_key,
#                                   producer_exchange, producer_routing_key, durable=True)


def task(body):
    print(body)
    time.sleep(5)
    return [json.dumps({'result': body})]


# 运行服务端
service.run(task, thread_count=2)
