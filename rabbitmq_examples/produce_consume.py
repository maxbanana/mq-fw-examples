# -*- coding: UTF-8 -*-
# @Time : 2022/5/10 下午3:52 
# @Author : 刘洪波
import rabbitmq
import json
"""
rabbitmq 默认使用topic模式，不能更改
该示例是 rabbitmq 调用端，先生产在消费
"""

host = '0.0.0.0'
port = 5672
username = ''
password = ''


consumer_exchange = ''
consumer_routing_key = ''
producer_exchange = consumer_exchange
producer_routing_key = ''

connect = rabbitmq.connect(host, port, username, password)

# 使用调用端, 推荐使用随机模式
# 随机模式：消费时 使用random_exchange, random_routing_key
service = connect.produce_consume(producer_exchange, producer_routing_key, durable=True)

# 消费时 使用固定exchange, routing_key
# service = connect.produce_consume(producer_exchange, producer_routing_key, consumer_exchange, consumer_routing_key,
#                                   durable=True)

msg_list = []
for i in range(5):
    msg_list.append(json.dumps({'data': i}))

# 运行调用端
result = service.run(msg_list, thread_count=2)
for i in result:
    print(i)
