# -*- coding: UTF-8 -*-
# @Time : 2022/4/29 01:08 
# @Author : HongBo Liu

import rabbitmq
"""
rabbitmq 默认使用topic模式，不能更改
"""

host = '0.0.0.0'
port = 5672
username = ''
password = ''
exchange = ''
routing_key = ''


# 连接rabbitmq
rq = rabbitmq.connect(host, port, username, password)

# 创建消费者
consumer = rq.create_consumer(exchange, routing_key)


def task(msg):
    import time
    time.sleep(5)
    print("接收 {} 成功.......".format(msg))


# 一直消费
consumer.receive(task)

# 消费一次， consume_num: 消费consume_num 次就停止消费
# consumer.receive(task, consume_num=1)
