# -*- coding: UTF-8 -*-
# @Time : 2022/5/10 下午4:43 
# @Author : 刘洪波
from loguru import logger
import RabbitmqPulsar
"""
Rabbitmq 与 Pulsar 互相连接
Rabbitmq 消费的消息发送至 pulsar
pulsar 消费的消息发送至 Rabbitmq
"""

# pulsar 配置
pulsar_url = ''
producer_topic = ''
consumer_topic = ''
consumer_name = ''

# rabbitmq 配置
host = ''
port = 5672
username = ''
password = ''
rb_mq_send_ex = ''
rb_mq_send_key = ''
rb_mq_cons_ex = ''
rb_mq_cons_key = ''


connect = RabbitmqPulsar.connect(host, port, username, password, pulsar_url)


"""
1.默认模式（消息从rabbitmq开始流转）：inter_services() 里 默认参数是 start_with_rabbitmq=True, random_queue=True
1). 从 rabbitmq 订阅，将数据发送至 pulsar; 再从 pulsar 订阅，将数据发送至 rabbitmq
2). 使用使用随机队列来生产消息
3). 消息数据流向： rabbitmq调用端 --> 本互联服务（rabbitmq服务端，pulsar调用端）--> pulsar服务端
"""
# 使用默认模式
service = connect.inter_services()

# 运行服务
service.run(producer_topic, rb_mq_cons_ex, rb_mq_cons_key, durable=True, thread_count=5, logger=logger)


"""
2.消息从pulsar开始流转模式：inter_services() 里参数是 start_with_rabbitmq=False, random_queue=True
1). 从 pulsar 订阅，将数据发送至 rabbitmq; 再从 rabbitmq 订阅，将数据发送至 pulsar
2). random_queue=True， 使用使用随机队列来生产消息
3). 消息数据流向： pulsar调用端 --> 本互联服务（pulsar服务端，rabbitmq调用端）--> rabbitmq服务端
"""
# 使用从pulsar开始流转模式
# service = connect.inter_services(start_with_rabbitmq=False)

# 运行服务
# service.run(consumer_topic, consumer_name, rb_mq_send_ex, rb_mq_send_key, durable=True, thread_count=5, logger=logger)


"""
3.通用模式：inter_services() 里 参数是 random_queue=False
1). 从 pulsar 订阅，将数据发送至 rabbitmq; 再从 rabbitmq 订阅，将数据发送至 pulsar
2). random_queue=False， 不使用使用随机队列来生产消息
3). 消息数据流向， 两条独立的数据流同时运行
    1. pulsar生产 --> 本互联服务（pulsar消费，rabbitmq生产）--> rabbitmq消费
    2. rabbitmq生产 --> 本互联服务（rabbitmq消费，pulsar生产）--> pulsar消费
"""
# 使用从pulsar开始流转模式
# service = connect.inter_services(random_queue=False)

# 运行服务
# service.run(producer_topic, consumer_topic, consumer_name,
#             rb_mq_send_ex, rb_mq_send_key, rb_mq_cons_ex, rb_mq_cons_key, durable=True, logger=logger)

