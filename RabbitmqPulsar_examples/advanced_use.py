# -*- coding: UTF-8 -*-
# @Time : 2022/5/10 下午10:40 
# @Author : 刘洪波
"""
进阶使用
Rabbitmq 与 Pulsar 互相连接
"""

from loguru import logger
import RabbitmqPulsar


# pulsar 配置
pulsar_url = ''
producer_topic = ''
consumer_topic = ''
consumer_name = ''

# rabbitmq 配置
host = ''
port = 5672
username = ''
password = ''
rb_mq_send_ex = ''
rb_mq_send_key = ''
rb_mq_cons_ex = ''
rb_mq_cons_key = ''


connect = RabbitmqPulsar.connect(host, port, username, password, pulsar_url)


# 使用默认模式（消息从rabbitmq开始流转）
service = connect.inter_services()


def rabbitmq_task(msg):
    """
    处理消费的rabbitmq消息，返回结果将发送至 pulsar
    :param msg: rabbitmq消息
    :return:
    """
    print('rabbitmq消息:', msg)
    return msg


def pulsar_task(msg):
    """
    处理消费的pulsar消息，返回结果将发送至 rabbitmq
    :param msg: pulsar消息
    :return:
    """
    print('pulsar消息:', msg)
    return [msg]


# 运行服务
service.run(producer_topic, rb_mq_cons_ex, rb_mq_cons_key, rabbitmq_task=rabbitmq_task, pulsar_task=pulsar_task,
            durable=True, thread_count=5, logger=logger)
