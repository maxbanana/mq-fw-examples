# -*- coding: UTF-8 -*-
# @Time : 2022/4/29 00:25 
# @Author : HongBo Liu
"""
生产消息
"""

import pulsar_mq
import json

# pulsar服务地址
pulsar_url = 'pulsar://0.0.0.0:6650'

# 生产的topic
produce_topic = ''

"""
1. 连接pulsar
"""
client = pulsar_mq.client(pulsar_url)

"""
2. 创建生产者
"""
producer = client.create_producer(produce_topic)


msg = {"data": {"a": "1", "b": "2"}}

"""
3.发送消息
默认参数： _async=True, callback=None, random_topic=None
_async: 是否异步发送消息， True异步发送， Flase 同步发送
callback: 异步发送时的回调函数
random_topic: 随机topic

"""
producer.send(json.dumps(msg))


# 一次发多条消息
# producer.send([json.dumps(msg), json.dumps(msg2)])
