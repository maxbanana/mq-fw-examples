# -*- coding: UTF-8 -*-
# @Time : 2022/4/29 00:39 
# @Author : HongBo Liu

"""
服务端
先消费消息，再生产消息
"""


import pulsar_mq
import json


client = pulsar_mq.client('pulsar://0.0.0.0:6650')
"""
默认使用随机队列模式
随机队列模式是：
消费的消息里带一个random_topic，
生产的消息网random_topic里发送

当传入参数 producer_topic，使producer_topic不等于None时，
生产的消息往 producer_topic里发送
"""
service = client.consume_produce(consumer_topic='', consumer_name='')


def task(msg):
    """
    回调函数
    :param msg:
    :return:
    """
    msg = json.loads(msg)
    print(msg)
    random_topic = msg.get('random_topic')
    print(random_topic)
    import time
    time.sleep(6)
    msg = {"data": {"a": "1", "b": "2"}}
    return json.dumps(msg)


service.run(task)
