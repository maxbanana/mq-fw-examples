# -*- coding: UTF-8 -*-
# @Time : 2022/4/29 00:01 
# @Author : HongBo Liu
"""
消费数据
"""
import pulsar_mq

# pulsar服务地址
pulsar_url = 'pulsar://0.0.0.0:6650'

# 消费者订阅的topic
consumer_topic = ''

# 消费者的名字
consumer_name = ''

"""1. 连接pulsar"""
client = pulsar_mq.client(pulsar_url)

"""
2. 创建消费者
默认参数 schema=pulsar.schema.StringSchema(), consumer_type='Shared'
'Shared': 共享模式
'Exclusive': 独占模式
'Failover': 灾备模式
'KeyShared': 关键字共享模式
"""
consumer = client.create_consumer(consumer_topic, consumer_name)


def task(msg):
    """
    3. 回调函数
     处理接收的消息
    :param msg:  消费的消息
    :return:
    """
    print(msg)


"""
4.开始消费
一直监听进行消费
默认参数 thread_count=None, logger=None
若设置thread_count=5 程序将开启5个线程进行消费
logger 日志收集器
"""
consumer.receive(task)

# 只消费一个就停止监听，关闭消费者
# consumer.receive_one(task)

# 关闭消费者
# consumer.close()

# 取消订阅，并关闭消费者
# consumer.unsubscribe()


