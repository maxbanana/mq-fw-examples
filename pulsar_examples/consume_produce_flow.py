# -*- coding: UTF-8 -*-
# @Time : 2022/5/20 下午6:53 
# @Author : 刘洪波

import pulsar_mq
import json
"""
流服务，用于组成服务流
1. 订阅pulsar
2. 处理消费的数据
3. 发送业务数据
   1）当服务正常运行无报错时将结果发送至下一个服务的topic，
   2）当服务运行异常有报错时将结果发送至收集error的topic。
4. 发送的 队列只能是固定的
"""
client = pulsar_mq.client('pulsar://0.0.0.0:6655')
service = client.consume_produce_flow(consumer_topic='test_p_001', consumer_name='test_p_001',
                                      producer_next_topic='test_p_002', producer_error_topic='test_p_e')


def task(msg):
    try:
        msg = json.loads(msg)
        print(msg)
        print(msg['data'])
        return json.dumps({'rr': msg['data']}), True
    except Exception as e:
        return json.dumps({'error': str(e)}), False


service.run(task)
