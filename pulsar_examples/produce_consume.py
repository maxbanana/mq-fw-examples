# -*- coding: UTF-8 -*-
# @Time : 2022/4/29 00:50 
# @Author : HongBo Liu
"""
调用端
先生产发送消息，然后消费消息
"""

import pulsar_mq
import json

client = pulsar_mq.client('pulsar://0.0.0.0:6650')

"""
默认使用随机队列模式
随机队列模式是：
当使用默认参数 consumer_topic=None, consumer_name=None时
pulsar_mq包自动生成一个random_topic，然后和消息一起发送出去，同时监听random_topic
服务端接收到消息后将本服务生产好的消息往random_topic发送。

当传入参数 consumer_topic、consumer_name时，不使用随机队列模式
"""
service = client.produce_consume(producer_topic='')


msg = {"data": {"a": "1", "b": "2"}}
msg2 = {"data": {"c": "3", "d": "4"}}

msg_list = [json.dumps(msg), json.dumps(msg2)]

data = service.run(msg_list)
print(data)
for d in data:
    print(d)


# 只发送一个消息
# data = service.run(json.dumps(msg))
# print(data)
