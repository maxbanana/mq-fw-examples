# mq-fw-examples

### 介绍
mq-fw包的例子

mq-fw 介绍：消息队列的框架，包含pulsar和 rabbitmq。

#### 功能
1. pulsar 订阅和消费
2. rabbitmq 订阅和消费
3. pulsar与rabbitmq 互相订阅和消费


#### 安装教程

pip install mq-fw

#### 使用说明

1.  使用 pulsar框架

        import pulsar_mq

2.  使用 rabbitmq框架

        import pulsar_mq

3.  使用 RabbitmqPulsar框架

        import RabbitmqPulsar

#### 使用的相关例子
1.  使用 pulsar框架的例子在 pulsar_examples 目录下

2.  使用 rabbitmq框架的例子在 rabbitmq_examples 目录下

3.  使用 RabbitmqPulsar框架的例子在 RabbitmqPulsar_examples 目录下